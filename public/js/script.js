"use strict";

function updatePrice() {
    // Находим select по имени в DOM.
    let s = document.getElementsByName("typeOfItem");
    let col = document.querySelector('.quantity');
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
      price = prices.prodTypes[priceIndex] * col.value;

    }
    
    // Скрываем или показываем радиокнопки.
    let radioDiv = document.querySelector(".radio");
    radioDiv.style.display = (select.value == "3" || select.value == "2" ? "block" : "none");
    
    // Смотрим какая товарная опция выбрана.
    let radio = document.getElementsByName("prodOptions");
    radio.forEach(function(radio) {
      if (radio.checked) {
        let optionPrice = prices.prodOptions[radio.value];
        if (optionPrice !== undefined) {
          price += optionPrice;
        }
      }
    });
  
    // Скрываем или показываем чекбоксы.
    let checkDiv = document.querySelector(".checkbox");
    checkDiv.style.display = (select.value == "3" ? "block" : "none" );
  
    // Смотрим какие товарные свойства выбраны.
    let checkboxes = document.getElementById("cb")
      if (checkboxes.checked) {
          console.log(checkboxes);
          let propPrice = 500;
        if (propPrice !== undefined) {
          price += propPrice;
        }
    };

    let btn = document.getElementById("btn");
    btn.addEventListener("click", (event) =>{
        event.preventDefault();
        let result = document.getElementById('result');
        result.innerHTML = price + " рублей";

    })
  
  function getPrices() {
    return {
      prodTypes: [500, 15000, 20000],
      prodOptions: {
        "Да": 200,
        "Нет": 0
      }
    };
  }
}
  
  window.addEventListener('DOMContentLoaded', function (event) {
    let radioDiv = document.querySelector('.radio');
    radioDiv.style.display = "none";
    let checkbox = document.querySelector('.checkbox');
    radioDiv.style.display = "none";
    // Находим select по имени в DOM.
    let s = document.getElementsByName("typeOfItem");
    let select = s[0];
    // Назначаем обработчик на изменение select.
    select.addEventListener("change", function(event) {
        console.log(event);
        let checkDiv = document.getElementById('cb');
        let target = event.target;
        let radio = document.getElementsByName("prodOptions");
        checkDiv.checked = false;
        radio.forEach(function(radio) {
            if (radio.checked) {
                radio.checked = false;
            }
        })
        let result = document.getElementById('result');
        result.innerHTML = "0" + " рублей";
        updatePrice();
    });

    let col = document.querySelector(".quantity");
    col.addEventListener("blur", (event) => {
        console.log(event);
        let target = event.target;
        updatePrice();
    })
    
    // Назначаем обработчик радиокнопок.  
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radio) {
      radio.addEventListener("change", function(event) {
        console.log(event);
        let r = event.target;
        console.log(r.value);
        updatePrice();
      });
    });
  
      // Назначаем обработчик радиокнопок.  
    let checkboxes = document.querySelectorAll("#cb")
    checkboxes.forEach(function(checkbox) {
      checkbox.addEventListener("change", function(event) {
        console.log(event);
        let c = event.target;
        console.log(c.name);
        console.log(c.value);
        updatePrice();
      });
    });
  
    updatePrice();
  });
    





